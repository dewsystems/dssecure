# DSSecure

This library contains easy-to-use implementations of the most used security functions in most applications.

## Compatibility

This library is compatible with .Net Core versions 1.0 and above. It can be used in ASP.Net Core, WPF.Net Core, WinForms, Console Apps and other class libraries.

## License

This software is licensed under the MIT license. See the [LICENSE.txt file](https://gitlab.com/dewsystems/dssecure/blob/master/LICENSE.txt "The MIT License") for a full version of the license terms.

## Contributing

This repository is open for pull requests, so feel free to contribute, fix issues, etc.
